#!/bin/sh

echo ""
echo "Creating certificates"
echo ""

rm -rf certs && mkdir certs
cd certs

mkcert isoa.local "*.isoa.local"
mv isoa.local+1-key.pem isoa.local.key
mv isoa.local+1.pem isoa.local.crt

