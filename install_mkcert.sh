#!/bin/sh

echo ""
echo "Updating package lists and installing dependencies for mkcert."
echo "Needs root privileges."
echo ""

sudo apt-get update
sudo apt-get install -y libnss3-tools

echo ""
echo "Downloading and installing mkcert."
echo ""

curl -JLO "https://dl.filippo.io/mkcert/latest?for=linux/amd64"
chmod +x mkcert-v*-linux-amd64
sudo mv mkcert-v*-linux-amd64 /usr/local/bin/mkcert

echo ""
mkcert -install
